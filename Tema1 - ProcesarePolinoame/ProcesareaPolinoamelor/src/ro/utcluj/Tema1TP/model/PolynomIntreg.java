package ro.utcluj.Tema1TP.model;

import java.util.ArrayList;
import java.util.List;

public class PolynomIntreg extends Polynom {
	
	/*
	 * 
	 * @param terms - lista de monoame a polinomului intreg
	 */
	private List<Monom> terms = new ArrayList<Monom>(); // lisa cu termenii polinomului
	
	
	/* Constructor polinom, adaugare monoame in lista */
	public PolynomIntreg (String s){
		super();
		Monom x = new MonomIntreg(0,0);
		if (s != null && !s.isEmpty())
		{		s = s.replace(" ", ""); 			//>3x^6+5x-10 
				String s1 = s.replace("-","+-");	//> s1=3x^6+5x+-10 // 15 // x // 2x^4
				String[] s2 = s1.split("\\+");		//> s2[0]=3x^6 s2[1]=5x s2[2]=-10
				for (int i=0; i < s2.length; i++)
				{	x = x.toMonom(s2[i]);
					this.terms.add(x);
				}
		}
	}
	
	public PolynomIntreg()  {}

	/* Adunarea a doua polinoame P1+P2 */
	public Polynom AddPolynoms(String s1, String s2)
	{	PolynomIntreg p1 = new PolynomIntreg(s1);
		PolynomIntreg p2 = new PolynomIntreg(s2);
		PolynomIntreg p3 = new PolynomIntreg("");
		p3 = p1.addition(p2);
		return p3;
		//view.setResultPolynomial( p3.toString() );
	}
	
	/* Scaderea a doua polinoame P1 - P2 */
	public Polynom SubtractPolynoms(String s1, String s2)
	{	PolynomIntreg p1 = new PolynomIntreg(s1);
		PolynomIntreg p2 = new PolynomIntreg(s2);
		PolynomIntreg p3 = new PolynomIntreg("");
		p3 = p1.subbtraction(p2);
		return p3;
	}
	
	/* Inmultirea a doua polinoame P1 * P2 */
	public Polynom MultiplicationPolynoms(String s1, String s2)
	{	PolynomIntreg p1 = new PolynomIntreg(s1);
		PolynomIntreg p2 = new PolynomIntreg(s2);
		PolynomIntreg p3 = new PolynomIntreg("");
		p3 = p1.multiplication(p2);
		return p3;
	}
	/* Impartirea a doua polinoame */
	public  ArrayList<Polynom> dividePolynoms (String s1, String s2)
	{	PolynomReal p1 = new PolynomReal(s1);
		PolynomReal p2 = new PolynomReal(s2);
		PolynomReal p3 = new PolynomReal("");
		PolynomReal p4 = new PolynomReal("");
		p3 = p1.impartirePolinom(p2,0);
		p4 = p1.impartirePolinom(p2,1);

		ArrayList<Polynom> result = new ArrayList<>();
		result.add(p3);
		result.add(p4);
		return result;
	}
	
	/* Integrarea unui polinom P1 */
	public Polynom IntegratePolynoms(String s1)
	{	PolynomReal p3 = new PolynomReal(s1);

		p3 = p3.integration();
		return p3;
	}
	
	/* Derivarea unui polinom P1 */
	public Polynom DerivatePolynoms(String s1)
	{	PolynomIntreg p1 = new PolynomIntreg(s1);
		p1 = p1.derivation();
		return p1;
	}
	
	/* Valoarea unui polinom P1 intr-un punct x . */
	public int SetValuePolynome(String s1, int x)
	{		PolynomIntreg p = new PolynomIntreg(s1);
			return p.valuePolynome(x);
	}
	
	/* Adaugare polinom1 in view */
	public Polynom SetFirstPolynom(String s1)
	{
		PolynomIntreg p1 = new PolynomIntreg(s1);
		p1.adunareMonoame();
		p1.ordonarePolinom();
		return p1;
	}
	
	
	/* Adaugare polinom2 in view */
	public Polynom SetSecondPolynom(String s1)
	{
		PolynomIntreg p1 = new PolynomIntreg(s1);
		p1.adunareMonoame();
		p1.ordonarePolinom();
		return p1;
	}
	
	
	/* Metoda de calcul a adunarii a doua polinoame */
	public PolynomIntreg addition (PolynomIntreg x)
	{	PolynomIntreg p = new PolynomIntreg("");
		for (int i=0; i<x.terms.size(); i++)
			 p.terms.add(x.terms.get(i));
		for (int j=0; j<this.terms.size(); j++)
			 p.terms.add(this.terms.get(j));
		p.adunareMonoame();
		p.ordonarePolinom();
		return p;
	}



	/* Metoda de calcul a scaderii dintre doua polinoame */
	public PolynomIntreg subbtraction (PolynomIntreg x)
	{	PolynomIntreg p = new PolynomIntreg("");
		for (int j=0; j<this.terms.size(); j++)
			 p.terms.add(this.terms.get(j));
		for (int i=0; i<x.terms.size(); i++)
		{	 x.terms.get(i).setCoef(0 - x.terms.get(i).getCoefficient().intValue());
			 p.terms.add(x.terms.get(i));
		}
		p.adunareMonoame();
		p.ordonarePolinom();
		return p;
	}
	
	
	/* Metoda de calcul a inmultirii a doua polinoame */
	public PolynomIntreg multiplication (PolynomIntreg x){
		PolynomIntreg p = new PolynomIntreg("");
		
		for (int i=0; i<this.terms.size(); i++)
			for (int j=0; j<x.terms.size(); j++)
				p.terms.add(this.terms.get(i).multiply(x.terms.get(j)));
		p.adunareMonoame();
		p.ordonarePolinom();
		return p;
	}


	/* Metoda de calcul a derivatei unui polinom */
	public PolynomIntreg derivation (){
		PolynomIntreg p = new PolynomIntreg("");
		for (int i=0; i<terms.size(); i++)
			p.terms.add(terms.get(i).derivation());
		p.adunareMonoame();
		p.ordonarePolinom();
		return p;
	}
	
	/* Metoda de calcul a valorii unui polinom intr-un punct c. */
	public int valuePolynome(int c){
		int res=0;
		for (int i=0; i<terms.size(); i++)
			res = (int) (res + terms.get(i).getCoefficient().intValue()*Math.pow(c,terms.get(i).getPower()));
		
		return res;
	}
	 
	

	/* Ordonarea coeficientilor unui polinom incepand cu cea mai mare putere */
	public void ordonarePolinom (){
			for (int i=0; i<this.terms.size(); i++)
			{	for (int j=i+1; j<this.terms.size() ; j++)
						if (this.terms.get(i).getPower() < this.terms.get(j).getPower())
						{	Monom tmp = this.terms.get(i);
							this.terms.set(i, this.terms.get(j));
							this.terms.set(j, tmp);
		
						}
			}
	}
	
	
	/* Adunarea monoamelor de acelasi grad in interiorul unui polinom */
	public void adunareMonoame (){
		for (int i=0; i< this.terms.size()-1; i++)
		{	for (int j=i+1; j<this.terms.size() ; j++)
					if (this.terms.get(i).getPower() == this.terms.get(j).getPower()) {
						this.terms.get(i).setCoef(this.terms.get(i).getCoefficient().intValue() + this.terms.get(j).getCoefficient().intValue());
						this.terms.remove(j);
						j--;
					}
		}
	}
	
	public int degreePolynom(){
		int max=0;
		for (Monom i: this.terms)
		{	if (i.getPower() > max)
				max = i.getPower();
		}
		return max;
	}


	/* toString method */
	@Override
	public String toString (){
		String polinom="";
		for (int i=0; i < terms.size(); i++)
		{	if (terms.get(i).getCoefficient().doubleValue() !=0)
				if(i==0)	
					polinom = polinom  + terms.get(i).toString();
				else 
					polinom = polinom  + "+" + terms.get(i).toString();
		}
		polinom = polinom.replace("+-","-");
		if (polinom.length()==0)
			polinom = polinom + "0";
		return polinom;
	}

}