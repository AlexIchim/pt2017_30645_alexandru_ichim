package ro.utcluj.Tema1TP.model;


//Clasa abstracta Monom din care rezulta MonomReal si MonomIntreg
/*
 * @param power - gradul monomului
 * @param coefficient - coeficientul monomului de tip Number : real/intreg
 */

public abstract class Monom {
	protected int power;
	protected Number coefficient;
	
	protected Monom ( Number inCoefficient, int inPower)
	{	this.power = inPower;
		this.coefficient = inCoefficient;
	}

	public abstract Monom addition(Monom m);

	public abstract Monom subtraction(Monom m);

	public abstract Monom multiply(Monom m);

	public abstract Monom division(Monom m);

	public abstract Monom toMonom(String s);


	public abstract Number getCoefficient();

	public void setCoef (Number coefficient ){
		this.coefficient = coefficient;
	}

	public int getPower (){
		return this.power;
	}

	public void setPower(int power){
		this.power = power;
	}

	public MonomReal integration (){
		return new MonomReal(this.power + 1, this.coefficient.doubleValue()/(this.power + 1));
	}

	public MonomIntreg derivation() {
		return power == 0 ? new MonomIntreg(0,0) : new MonomIntreg(this.coefficient.intValue() * power, power - 1);
	}
}
	