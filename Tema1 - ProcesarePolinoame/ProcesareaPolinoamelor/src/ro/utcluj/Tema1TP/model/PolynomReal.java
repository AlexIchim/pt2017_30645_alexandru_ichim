package ro.utcluj.Tema1TP.model;

import java.util.ArrayList;
import java.util.List;

public class PolynomReal extends Polynom  {
	
	private List<Monom> terms = new ArrayList<Monom>(); // lisa cu termenii polinomului

	
	public PolynomReal (String s){
		super();
		Monom x = new MonomReal(0,0);
		if (s != null && !s.isEmpty())
		{		s = s.replace(" ", ""); 			//>3x^6+5x-10 
				String s1 = s.replace("-","+-");	//> s1=3x^6+5x+-10 // 15 // x // 2x^4
				String[] s2 = s1.split("\\+");		//> s2[0]=3x^6 s2[1]=5x s2[2]=-10
				for (int i=0; i < s2.length; i++)
				{
					s2[i] = s2[i].replaceAll("0","");
					//s2[i] = numberFormat.format(s2[i]);
					s2[i] = s2[i].replaceAll(",", "");
					//System.out.println("Monom nr: " + i + " : " + s2[i]);
					x = x.toMonom(s2[i]);
					this.terms.add(x);
				}
		}
	}

	
	/* Metoda de calcul a impartirii a doua polinoame */
	public PolynomReal impartirePolinom (PolynomReal y, int op){
		PolynomReal zero = new PolynomReal("");
		int gradX = this.degreePolynom();
		int gradY = y.degreePolynom();
		int step = gradX - gradY + 1;
		int i=0;

		PolynomReal cat = new PolynomReal("");
		//System.out.println("This string : "  + this.toString() + " !!!");

		PolynomReal rest = new PolynomReal(this.toString());

		if (gradY > gradX)
		{	cat = zero; // catul
			rest = this;  // restul
		}
		else
		{	while (step != 0)
			{
			
				if (gradX >=  y.degreePolynom())
                {
					cat.terms.add(rest.terms.get(i).division(y.terms.get(0)));
					PolynomReal p = new PolynomReal("");
					String test = cat.terms.get(i).toString();
					PolynomReal c = new PolynomReal(cat.terms.get(i).toString());
					p = c.multiplication(y);
					rest = rest.subbtraction(p);
					if (rest.terms.get(i).getCoefficient().doubleValue() < 0.01 && rest.terms.get(i).getCoefficient().doubleValue() > -0.01)
						rest.terms.get(i).setCoef(0);
					PolynomReal q = new PolynomReal(rest.toString());
					gradX = q.degreePolynom();
					if (gradX == 0 && q.toString().equals("0"))
						break;
					i++;
				}
				step--;
			}
		}		
		for (Monom l : rest.terms)
		{	if ( l.getCoefficient().doubleValue() < 0.01 && l.getCoefficient().doubleValue() > -0.01)
			{
					l.setCoef(0);
			}
		}		
		
		if (op == 0)
			return cat;
		else
			return rest;
	} 
	
	
	/* Metoda de calcul a integrarii unui polinom */
	public PolynomReal integration (){
		this.adunareMonoame();
		this.ordonarePolinom();
		PolynomReal p = new PolynomReal("");
		for (int i=0; i<this.terms.size(); i++)
		{	p.terms.add(this.terms.get(i).integration());
			
		}
		p.adunareMonoame();
		p.ordonarePolinom();
		return p;
	}
	
	public PolynomReal impartireConstanta(double b){		
		PolynomReal p = new PolynomReal("");
		for (int i=0; i<this.terms.size(); i++)
		{	this.terms.get(i).setCoef(this.terms.get(i).getCoefficient().doubleValue()/b);
			p.terms.add(this.terms.get(i));
		}
		return p;
	}
	

	/* Metoda de calcul a scaderii dintre doua polinoame */
	public PolynomReal subbtraction (PolynomReal x)
	{	PolynomReal p = new PolynomReal("");
		for (int j=0; j<this.terms.size(); j++)
			 p.terms.add(this.terms.get(j));
		for (int i=0; i<x.terms.size(); i++)
		{	 x.terms.get(i).setCoef( 0 - (x.terms.get(i).getCoefficient().doubleValue()));
			 p.terms.add(x.terms.get(i));
		}
		p.adunareMonoame();
		p.ordonarePolinom();
		return p;
	}
	
	
	/* Metoda de calcul a inmultirii a doua polinoame */
	public PolynomReal multiplication (PolynomReal x){
		PolynomReal p = new PolynomReal("");
		
		for (int i=0; i<this.terms.size(); i++)
			for (int j=0; j<x.terms.size(); j++)
				p.terms.add(this.terms.get(i).multiply(x.terms.get(j)));
		p.adunareMonoame();
		p.ordonarePolinom();
		return p;
	}
		
	
	public void ordonarePolinom (){
			for (int i=0; i<this.terms.size(); i++)
			{	for (int j=i+1; j<this.terms.size() ; j++)
						if (this.terms.get(i).getPower() < this.terms.get(j).getPower())
						{	Monom tmp = this.terms.get(i);
							this.terms.set(i, this.terms.get(j));
							this.terms.set(j, tmp);
		
						}
			}
	}
	
	public void adunareMonoame (){
		for (int i=0; i< this.terms.size()-1; i++)
		{	for (int j=i+1; j<this.terms.size() ; j++)
					if (this.terms.get(i).getPower() == this.terms.get(j).getPower())
					{	this.terms.get(i).setCoef( this.terms.get(i).getCoefficient().doubleValue() +  this.terms.get(j).getCoefficient().doubleValue());
						this.terms.remove(j);
						j--;
					}
					
		}
	}
		
    public int degreePolynom(){
        int max=0;
        for (Monom i: this.terms)
        {	if (i.getPower() > max)
                max = i.getPower();
        }
        return max;
    }


    @Override
    public List<Monom> getTerms() {
        return terms;
    }

    @Override
    public void setTerms(List<Monom> terms) {
        this.terms = terms;
    }

    @Override
	public String toString (){
		String polinom="";
		for (int i=0; i < terms.size(); i++)
		{	if (terms.get(i).getCoefficient().doubleValue() !=0)
				if(i==0)	
					polinom = polinom  + terms.get(i).toString();
				else 
					polinom = polinom  + "+" + terms.get(i).toString();
		}
		polinom = polinom.replace("+-","-");
		if (polinom.length()==0)
			polinom = polinom + "0";
		return polinom;
	}
}