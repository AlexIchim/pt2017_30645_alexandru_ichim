package ro.utcluj.Tema1TP.model;

import java.util.ArrayList;
import java.util.List;

	public abstract class Polynom {

		//private int degree ; // n gradul polinomului
		private List<Monom> terms ; // lisa cu termenii polinomului

		public Polynom(){
		this.terms= new ArrayList<Monom>();
	}

	//public abstract Polynom multiplication(Polynom x);

	//public abstract Polynom subbtraction(Polynom x);

	//public abstract void adunareMonoame();
	
	
	public void ordonarePolinom (){
		for (int i=0; i<this.terms.size(); i++)
		{	for (int j=i+1; j<this.terms.size() ; j++)
					if (this.terms.get(i).getPower() < this.terms.get(j).getPower())
					{	Monom tmp = this.terms.get(i);
						this.terms.set(i, this.terms.get(j));
						this.terms.set(j, tmp);
	
					}
		}
	}
	
	/* Gradul unui polinom */
	public int degreePolynom(){
		int max=0;
		for (Monom i: this.terms)
		{	if (i.getPower() > max)
				max = i.getPower();
		}
		return max;
	}

	public List<Monom> getTerms() {
		return terms;
	}

	public void setTerms(List<Monom> terms) {
		this.terms = terms;
	}

	@Override
	public String toString (){
		String s="";
		return s;
	}
}