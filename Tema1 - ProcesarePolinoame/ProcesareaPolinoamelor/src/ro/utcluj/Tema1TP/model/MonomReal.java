package ro.utcluj.Tema1TP.model;

public class MonomReal extends Monom {
	
	/* Constructorul implicit apelat din clasa parinte */
	public MonomReal(int inPower, double coefficient)
	{
		super(coefficient, inPower);
	}
	
	/* Returneaza coeficientul real al monomului real */
	public double getCoef (){
		return coefficient.doubleValue();
	}

	@Override
	public Number getCoefficient() {
		return this.coefficient.doubleValue();
	}

	@Override
	public Monom addition(Monom b) {
		return new MonomReal(this.power, this.getCoefficient().doubleValue() + b.getCoefficient().doubleValue());
	}

	@Override
	public Monom subtraction(Monom m) {
		Monom p = new MonomReal(0,0);
		if((this.getCoefficient().doubleValue() - m.getCoefficient().doubleValue()) == 0)
			return p;
		else
		{			p.setCoef(this.getCoefficient().doubleValue() - m.getCoefficient().doubleValue());
			p.setPower(m.getPower());
		}
		if (p.getCoefficient().doubleValue() < 0.01)
			p.setCoef(0);
		return p;
	}

	@Override
	public Monom multiply (Monom b) {
		return new MonomReal(this.getPower() + b.getPower(), this.getCoefficient().doubleValue()*b.getCoefficient().doubleValue());
	}

	@Override
	public Monom division(Monom m) {
		return new MonomReal(this.getPower() - m.getPower(), this.getCoefficient().doubleValue() / m.getCoefficient().doubleValue());
	}



	/* Converteste un string s intr-un monom intreg 
	 * 	String de forma: 3x^2 / 2x / 5x^3 / -3x / 2x^9  etc*/
	public MonomReal toMonom (String s){
		MonomReal p = new MonomReal(0,0);
		String[] s1;
		if (s!= null && !s.isEmpty())
		{	if (s.toLowerCase().contains("x^"))
			{	s1 = s.split("x\\^");
				if (s.startsWith("x"))
				{	p.coefficient = 1.0;			
					p.power = Integer.parseInt(s1[1]);
				}
				else
					if (s.startsWith("-x"))
					{	p.coefficient = -1.0;			
						p.power = Integer.parseInt(s1[1]);
					}	
					else
					{	p.coefficient = Double.parseDouble(s1[0]);			
						p.power = Integer.parseInt(s1[1]);
					}
			}
			else
					if (s.toLowerCase().contains("x"))	
					{	if (s.equals("x"))
						{	p.power = 1;
							p.coefficient = 1.0;
						}
						else
							if(s.equals("-x"))
							{	p.power = 1;
								p.coefficient = -1.0;
							}
							else
							{	s1 = s.split("x");
								p.power = 1;
								p.coefficient = Double.parseDouble(s1[0]);
							}
					}
					else
						if (Double.parseDouble(s) != 0)
						{	p.coefficient = Double.parseDouble(s);
							p.power = 0;
						}
		}
		return p;
	}
	
	public MonomReal changeSign(){
			this.coefficient= 0 - this.getCoef();
			return this;
	}
	
	/* Metoda toString de afisare */
	@Override
	public String toString (){
		String coef = "";
		  if (coefficient.doubleValue() == 0) {
			  return "0";
		  } 
		  else 
			  if (coefficient.doubleValue() == -1) {
				  	if (power == 0) {
				  			return "-1";
				  	} 
				  	else {	coef += "-";
				  	}
			  } 
			  else 

				  	if (coefficient.doubleValue() != 1) {
				  			coef += String.format("%3.3f", coefficient);
				  	} 
				  	else {	if (power == 0) {
				  				return "1";
				  		 }
				 }		

		  if (power != 0) {
			  if (power == 1) {
				  coef += "x";
			  } 
		  else {	coef += "x^" + String.valueOf(power);
		    }
		 }
		  return coef;
	}
}
