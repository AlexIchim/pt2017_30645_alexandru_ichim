package ro.utcluj.Tema1TP.model;

public class MonomIntreg extends Monom {
	
	
	/* Constructorul implicit apelat din clasa parinte */
	public MonomIntreg(int inPower, int coefficient)
	{
		super(inPower, coefficient);
	}


	@Override
	public Number getCoefficient() {
		return this.coefficient.intValue();
	}


	@Override
	public Monom addition(Monom b) {
		Monom p = new MonomIntreg(this.power, this.coefficient.intValue() + b.getCoefficient().intValue());
		return p;
	}

	/* Returneaza rezultatul scaderii dintre doua monoame intregi */
	@Override
	public Monom subtraction (Monom b){
		Monom p = new MonomIntreg(0,0);
		if((this.coefficient.intValue() - b.coefficient.intValue()) == 0)
			return p;
		else
		{		p.setCoef(this.getCoefficient().intValue() - b.getCoefficient().intValue());
			p.setPower(b.getPower());
		}
		return p;
	}

	/* Returneaza produsul a doua monoame intregi */
	@Override
	public Monom multiply (Monom b) {
		return new MonomIntreg( this.getCoefficient().intValue() * b.getCoefficient().intValue(), this.power + b.getPower());
	}

	@Override
	public Monom division(Monom m) {
		return new MonomIntreg(this.power - m.getPower(), this.getCoefficient().intValue() / m.getCoefficient().intValue());
	}

	
	/* Converteste un string s intr-un monom intreg 
	 * 	String de forma: 3x^2 / 2x / 5x^3 / -3x / 2x^9  etc*/
	public MonomIntreg toMonom (String s){
		MonomIntreg p = new MonomIntreg(0,0);
		String[] s1;
		if (s!= null && !s.isEmpty())
		{	if (s.toLowerCase().contains("x^"))
			{	s1 = s.split("x\\^");
				if (s.startsWith("x"))
				{	p.coefficient = 1;			
					p.power = Integer.parseInt(s1[1]);
				}
				else
					if (s.startsWith("-x"))
					{	p.coefficient = -1;			
						p.power = Integer.parseInt(s1[1]);
					}	
					else
					{	p.coefficient = Integer.parseInt(s1[0]);			
						p.power = Integer.parseInt(s1[1]);
					}
			}
			else
					if (s.toLowerCase().contains("x"))	
					{	if (s.equals("x"))
						{	p.power = 1;
							p.coefficient = 1;
						}
						else
							if(s.equals("-x"))
							{	p.power = 1;
								p.coefficient = -1;
							}
							else
							{	s1 = s.split("x");
								p.power = 1;
								p.coefficient = Integer.parseInt(s1[0]);
							}
					}
					else
						if (Integer.parseInt(s) != 0)
						{	p.coefficient = Integer.parseInt(s);
							p.power = 0;
						}
		
		}
		return p;
	}
	
	
	/* Metoda toString ptr afisarea monomului */
	@Override
	public String toString (){
		String coef = "";

		  if (coefficient.intValue() == 0) {
			  return "0";
		  } 
		  else 
			  if (coefficient.intValue() == -1) {
				  	if (power == 0) {
				  			return "-1";
				  	} 
				  	else {	coef += "-";
				  	}
			  } 
			  else 
				  	if (coefficient.intValue() != 1) {
				  			coef += String.valueOf(coefficient);
				  	} 
				  	else {	if (power == 0) {
				  				return "1";
				  		 }
				 }		

		  if (power != 0) {
			  if (power == 1) {
				  coef += "x";
			  } 
		  else {	coef += "x^" + String.valueOf(power);
		    }
		 }

		  return coef;
	}
}


