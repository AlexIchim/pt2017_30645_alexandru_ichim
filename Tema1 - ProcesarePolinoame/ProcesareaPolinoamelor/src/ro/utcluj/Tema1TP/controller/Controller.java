package ro.utcluj.Tema1TP.controller;


import ro.utcluj.Tema1TP.view.View;
import ro.utcluj.Tema1TP.model.PolynomIntreg;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/*
 * Controller-ul este folosit pentru a face legatura dintre Model(PolynomeIntreg) si View(view).
 * @param view - interfata grafica
 * @paramm Polynomial - clasa de efectuare a operatiilor pe polinoame intregi
 */


public class Controller {

	private View view;
	private PolynomIntreg Polynomial;
	
	
	public Controller(View view, PolynomIntreg Polynomial) {
		this.view = view;
		this.Polynomial = Polynomial;
		AddActionListeners();
	}
	
	private void AddActionListeners() {
		view.addbtnFristPolynomialOk (new FirstPolynomialOkListener());
		view.addbtnSecondPolynomialOkListener(new SecondPolynomialOkListener());
		view.addbtnAddPolynomialListener(new AddPolynomialListener());
		view.addbtnSubPolynomial1Listener(new SubPolynomial1Listener());
		view.addbtnSubPolynomial2Listener(new SubPolynomial2Listener());
		view.addbtnMultiplyListener(new MultiplyPolynomialListener());
		view.addbtnIntegration1Listener(new Integr1PolynomialListener());
		view.addbtnIntegration2Listener(new Integr2PolynomialListener());
		view.addbtnDerivation1Listener(new Deriv1PolynomialListener());
		view.addbtnDerivation2istener(new Deriv2PolynomialListener());
		view.addbtnValue1Listener(new Value1PolynomialListener());
		view.addbtnValue2Listener(new Value2PolynomialListener());
		view.addbtnDivide1Listener(new Divide1PolynomialListener());
		view.addbtnDivide2Listener(new Divide2PolynomialListener());
	}
	
	
	/* Activitatea efectuata de butonul OK din dreptul primului polinom */
	class FirstPolynomialOkListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent arg0) {
			try {
				view.setFirstPolynomial(Polynomial.SetFirstPolynom(view.getFirstPolynomial()).toString());
				view.setRemainderPolynomial("");
			} catch (NumberFormatException ex) {
				JOptionPane.showMessageDialog(null, "Please enter a valid polynomial !  Ex: ax^b + cx^d");
				view.setFirstPolynomial("");
			}
		}
	}
		
	/* Activitatea efectuata de butonul OK din dreptul celui de-al doilea polinom */
	class SecondPolynomialOkListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent arg0) {
			try {
				view.setSecondPolynomial(Polynomial.SetSecondPolynom(view.getSecondPolynomial()).toString());
			} catch (NumberFormatException ex) {
				JOptionPane.showMessageDialog(null, "Please enter a valid polynomial !  Ex: ax^b + cx^d");
			}
			view.setRemainderPolynomial("");
		}
	}
	
	
	/* Activitatea efectuata de butonul P1+P2 - adunarea a doua polinoame */
	class AddPolynomialListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent arg0) {
			view.setResultPolynomial(Polynomial.AddPolynoms(view.getFirstPolynomial(), view.getSecondPolynomial()).toString());
			view.setRemainderPolynomial("");
		}
	}
	
	/* Activitatea efectuata de butonul P1-P2 - scaderea a doua polinoame */
	class SubPolynomial1Listener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent arg0) {
			view.setResultPolynomial(Polynomial.SubtractPolynoms(view.getFirstPolynomial(), view.getSecondPolynomial()).toString());
			view.setRemainderPolynomial("");
		}
	}
	
	/* Activitatea efectuata de butonul P2-P1 - scaderea a doua polinoame */
	class SubPolynomial2Listener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent arg0) {
			view.setResultPolynomial(Polynomial.SubtractPolynoms(view.getSecondPolynomial(), view.getFirstPolynomial()).toString());
			view.setRemainderPolynomial("");
		}
	}
	
	/* Activitatea efectuata de butonul P1*P2 - inmultirea a doua polinoame */
	class MultiplyPolynomialListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent arg0) {
			view.setResultPolynomial(Polynomial.MultiplicationPolynoms(view.getFirstPolynomial(), view.getSecondPolynomial()).toString());
			view.setRemainderPolynomial("");
		}
	}
	
	/* Activitatea efectuata de butonul ∫P1 - integrarea primului polinom */
	class Integr1PolynomialListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent arg0) {
			view.setResultPolynomial(Polynomial.IntegratePolynoms(view.getFirstPolynomial()).toString());
			view.setRemainderPolynomial("");
		}
	}	
	
	/* Activitatea efectuata de butonul ∫P2 - integrarea celui de-al doilea polinom */
	class Integr2PolynomialListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent arg0) {
			view.setResultPolynomial(Polynomial.IntegratePolynoms(view.getSecondPolynomial()).toString());
			view.setRemainderPolynomial("");
		}
	}
	
	
	/* Activitatea efectuata de butonul P1' - derivarea primului polinom */
	class Deriv1PolynomialListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent arg0) {
			view.setResultPolynomial(Polynomial.DerivatePolynoms(view.getFirstPolynomial()).toString());
			view.setRemainderPolynomial("");
		}
	}
	
	/* Activitatea efectuata de butonul P2' - derivarea celui de-al doilea polinom */
	class Deriv2PolynomialListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent arg0) {
			view.setResultPolynomial(Polynomial.DerivatePolynoms(view.getSecondPolynomial()).toString());
			view.setRemainderPolynomial("");
		}
	}
	
	
	/* Activitatea efectuata de butonul P1(x) - valoarea lui P1 in punctul x */
	class Value1PolynomialListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent arg0) {
			try {
				view.setResultPolynomial(String.valueOf(Polynomial.SetValuePolynome(view.getFirstPolynomial(), view.getPolynomialPoint())));
			} catch (NumberFormatException ex) {
				JOptionPane.showMessageDialog(null, "Please enter x value !");
			}
			view.setRemainderPolynomial("");
		}
	}
	
	
	/* Activitatea efectuata de butonul P2(x) - valoarea lui P2 in punctul x */
	class Value2PolynomialListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent arg0) {
			try {
				view.setResultPolynomial(String.valueOf(Polynomial.SetValuePolynome(view.getSecondPolynomial(), view.getPolynomialPoint())));
			} catch (NumberFormatException ex) {
				JOptionPane.showMessageDialog(null, "Please enter x value !");
			}
			view.setRemainderPolynomial("");
		}
	}
	
	/* Activitatea efectuata de butonul P1/P2 - impartirea a doua polinoame */
	class Divide1PolynomialListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent arg0) {
			view.setResultPolynomial(Polynomial.dividePolynoms(view.getFirstPolynomial(),view.getSecondPolynomial()).get(0).toString());
			view.setRemainderPolynomial(Polynomial.dividePolynoms(view.getFirstPolynomial(),view.getSecondPolynomial()).get(1).toString());
		}
	}
	
	
	/* Activitatea efectuata de butonul P2/P1 - impartirea a doua polinoame */
	class Divide2PolynomialListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent arg0) {
			view.setResultPolynomial(Polynomial.dividePolynoms(view.getSecondPolynomial(),view.getFirstPolynomial()).get(0).toString());
			view.setRemainderPolynomial(Polynomial.dividePolynoms(view.getSecondPolynomial(),view.getFirstPolynomial()).get(1).toString());
		}
	}
}
