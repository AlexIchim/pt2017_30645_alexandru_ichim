package ro.utcluj.Tema1TP.view;

import ro.utcluj.Tema1TP.controller.Controller;
import ro.utcluj.Tema1TP.model.Monom;
import ro.utcluj.Tema1TP.model.PolynomIntreg;

public class GUI {
	
	/*
	 * Creare view @view
	 * Creare model @polinom
	 * Creare controller @control
	 */
	public static void main(String[] args)
	{
		View viewPolynom = new View(); // view
		PolynomIntreg modelPolynom = new PolynomIntreg(); // model
		Controller controllerPolynom = new Controller(viewPolynom, modelPolynom); //  controller
	}

}
