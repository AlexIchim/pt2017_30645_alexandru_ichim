package ro.utcluj.Tema1TP.view;

import java.awt.EventQueue;

import javax.swing.JFrame;
import java.awt.Color;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Font;

public class View {

	private JFrame frame;
	private JTextField firstPolynomial; 			//camp text  pentru introducerea a primului polinom
	private JTextField secondPolynomial;			//camp text  pentru introducerea celui de-al doilea polinom
	private JTextField viewFirstPolynomial;			//camp text  pentru afisarea primului polinom daca a fost introdus valid
	private JTextField viewSecondPolynomial;		//camp text  pentru afisarea celui de-al doilea polinom daca a fost introdus valid
	private JTextField resultPolyonomial;			//camp text de afisare a rezultatului pe polinoame
	private JTextField pointPolynomial;				//camp text pentru introducerea valorii X la calculul operatiei de gasire a valorii polinomului in punctul X
	private JTextField remainderPolynomial;			//camp text pentru restul impartirii polinoamelor

	private JLabel lblFirstPolynomial;				
	private JLabel lblSecondPolynomial;
	private JLabel lblViewFirstPolynomial;
	private JLabel lblViewSecondPolynomial;
	private JLabel lblResult;
	private JLabel lblRemainder;
	private JLabel lblValue;
	private JLabel lbltxt3;
	private JLabel lbltxt2;
	private JLabel lbltxt1;

	
	private JButton btnFristPolynomialOk;			//Validare primul polinom
	private JButton btnSecondPolynomialOk;			//Validare al doilea polinom
	private JButton btnAddPolynomial;				//Adunarea a doua polinoame P1+P2
	private JButton btnSubPolynomial1;				//Scaderea a doua polinoame P1-P2
	private JButton btnSubPolynomial2;				//Scaderea a doua polinoame P2-P1
	private JButton btnMultiply;					//Inmultirea a doua polinoame P1*P2
	private JButton btnDivide1;						//Impartirea a doua polinoame P1/P2
	private JButton btnDivide2;						//Impartirea a doua polinoame P2/P1
	private JButton btnIntegration1;				//Integrare P1
	private JButton btnIntegration2;				//Integrare P2
	private JButton btnDerivation1;					//Derivare P1
	private JButton btnDerivation2;					//Derivare P2
	private JButton btnValue1;						//Valoare P1(x)
	private JButton btnValue2;						//Valoare P2(x)


	/**
	 * Create the application.
	 */
	public View () {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		
		//creare window cu layout absolut pentru pozitionarea componenetelor
		frame = new JFrame("Procesarea Polinoamelor");
		frame.setBounds(100, 100, 600, 450);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		lblFirstPolynomial = new JLabel("Introduceti primul polinom (P1)");
		lblFirstPolynomial.setBounds(30, 10, 190, 15);
		frame.getContentPane().add(lblFirstPolynomial);
		
		
		firstPolynomial = new JTextField();
		firstPolynomial.setBounds(30, 25, 165, 30);
		frame.getContentPane().add(firstPolynomial);
		firstPolynomial.setColumns(10);
		
		btnFristPolynomialOk= new JButton("ok");
		btnFristPolynomialOk.setBounds(200, 25, 60, 30);
		frame.getContentPane().add(btnFristPolynomialOk);
		
		viewFirstPolynomial = new JTextField();
		viewFirstPolynomial.setEditable(false);
		viewFirstPolynomial.setColumns(10);
		viewFirstPolynomial.setBounds(300, 25, 165, 30);
		frame.getContentPane().add(viewFirstPolynomial);
		
		secondPolynomial = new JTextField();
		secondPolynomial.setColumns(10);
		secondPolynomial.setBounds(30, 85, 165, 30);
		frame.getContentPane().add(secondPolynomial);
		
		btnSecondPolynomialOk = new JButton("ok");
		btnSecondPolynomialOk.setBounds(200, 85, 60, 30);
		frame.getContentPane().add(btnSecondPolynomialOk);

		
		viewSecondPolynomial = new JTextField();
		viewSecondPolynomial.setEditable(false);
		viewSecondPolynomial.setColumns(10);
		viewSecondPolynomial.setBounds(300, 85, 165, 30);
		frame.getContentPane().add(viewSecondPolynomial);
	
			
		lblValue = new JLabel("X =");
		lblValue.setBounds(500, 85, 25, 30);
		frame.getContentPane().add(lblValue);
		
		pointPolynomial = new JTextField();
		pointPolynomial.setColumns(10);
		pointPolynomial.setBounds(530, 85, 35, 30);
		frame.getContentPane().add(pointPolynomial);
		
		resultPolyonomial = new JTextField();
		resultPolyonomial.setEditable(false);
		resultPolyonomial.setColumns(10);
		resultPolyonomial.setBounds(85, 285, 365, 30);
		frame.getContentPane().add(resultPolyonomial);
	
		
		remainderPolynomial= new JTextField();
		remainderPolynomial.setEditable(false);
		remainderPolynomial.setColumns(10);
		remainderPolynomial.setBounds(85, 325, 365, 30);
		frame.getContentPane().add(remainderPolynomial);
		
		
		
		lblSecondPolynomial = new JLabel("Introduceti al doilea polinom (P2)");
		lblSecondPolynomial.setBounds(30, 70, 220, 15);
		frame.getContentPane().add(lblSecondPolynomial);
		
		lblRemainder = new JLabel("Rest");
		lblRemainder.setBounds(30, 330, 50, 15);
		frame.getContentPane().add(lblRemainder);

		
		lblResult = new JLabel("Rezultat");
		lblResult.setBounds(30, 290, 50, 15);
		frame.getContentPane().add(lblResult);
	
		lblViewFirstPolynomial= new JLabel("Primul Polinom");
		lblViewFirstPolynomial.setBounds(300, 10, 150, 15);
		frame.getContentPane().add(lblViewFirstPolynomial);

		lblViewSecondPolynomial = new JLabel("Al doilea polinom");
		lblViewSecondPolynomial.setBounds(300, 70, 150, 15);
		frame.getContentPane().add(lblViewSecondPolynomial);
	
		
		
		

		
		
					
		btnAddPolynomial = new JButton("P1 + P2");
		btnAddPolynomial.setFont(new Font("Tahoma", Font.PLAIN, 9));
		btnAddPolynomial.setBounds(30, 150, 70, 30);
		frame.getContentPane().add(btnAddPolynomial);
		
		btnSubPolynomial1 = new JButton("P1 - P2");
		btnSubPolynomial1.setFont(new Font("Tahoma", Font.PLAIN, 9));
		btnSubPolynomial1.setBounds(30, 185, 70, 30);
		frame.getContentPane().add(btnSubPolynomial1);	
		
		btnSubPolynomial2 = new JButton("P2 - P1");
		btnSubPolynomial2.setFont(new Font("Tahoma", Font.PLAIN, 9));
		btnSubPolynomial2.setBounds(30, 219, 70, 30);
		frame.getContentPane().add(btnSubPolynomial2);
		
		btnMultiply = new JButton("P1 * P2");
		btnMultiply.setFont(new Font("Tahoma", Font.PLAIN, 9));
		btnMultiply.setBounds(105, 150, 70, 30);
		frame.getContentPane().add(btnMultiply);
		
		btnDivide1 = new JButton("P1  / P2");
		btnDivide1.setFont(new Font("Tahoma", Font.PLAIN, 9));
		btnDivide1.setBounds(105, 185, 70, 30);
		frame.getContentPane().add(btnDivide1);
		
		btnDivide2 = new JButton("P2 / P1");
		btnDivide2.setFont(new Font("Tahoma", Font.PLAIN, 9));
		btnDivide2.setBounds(105, 220, 70, 30);
		frame.getContentPane().add(btnDivide2);
		
		btnIntegration1 = new JButton("\u222BP1");
		btnIntegration1.setFont(new Font("Tahoma", Font.PLAIN, 9));
		btnIntegration1.setBounds(180, 150, 70, 30);
		frame.getContentPane().add(btnIntegration1);
		
		btnIntegration2 = new JButton("\u222BP2");
		btnIntegration2.setFont(new Font("Tahoma", Font.PLAIN, 9));
		btnIntegration2.setBounds(180, 185, 70, 30);
		frame.getContentPane().add(btnIntegration2);
		
		btnDerivation1 = new JButton("P1'");
		btnDerivation1.setFont(new Font("Tahoma", Font.PLAIN, 9));
		btnDerivation1.setBounds(180, 220, 70, 30);
		frame.getContentPane().add(btnDerivation1);
		
		btnDerivation2 = new JButton("P2'");
		btnDerivation2.setFont(new Font("Tahoma", Font.PLAIN, 9));
		btnDerivation2.setBounds(255, 150, 70, 30);
		frame.getContentPane().add(btnDerivation2);	
		
		btnValue1 = new JButton("P1(X)");
		btnValue1.setFont(new Font("Tahoma", Font.PLAIN, 9));
		btnValue1.setBounds(255, 185, 70, 30);
		frame.getContentPane().add(btnValue1);
		
		btnValue2 = new JButton("P2(X)");
		btnValue2.setFont(new Font("Tahoma", Font.PLAIN, 9));
		btnValue2.setBounds(255, 220, 70, 30);
		frame.getContentPane().add(btnValue2);
		
		lbltxt1 = new JLabel("Polinoamele se introduc de forma: (ax^b + cx + d / ax^2 + cx^1 + dx^0)   - a,b,c,d - intregi");
		lbltxt1.setBounds(30, 360, 510, 15);
		frame.getContentPane().add(lbltxt1);
		
		lbltxt2 = new JLabel("Pentru a calcula P1(X) si P2(X) , nu uitati sa introduceti o valoare in casuta X= .");
		lbltxt2.setBounds(30, 380, 510, 15);
		frame.getContentPane().add(lbltxt2);
		
		lbltxt3 = new JLabel("<html><center>Dupa ce ati introdus un polinom,<br> apasand pe butonul OK se verifica <br> daca acesta a fost introdus valid, <br> daca DA acesta va fi scris <br> in casuta text imediat urmatoare.<br></center>");
		lbltxt3.setBounds(370, 150, 220, 100);
		frame.getContentPane().add(lbltxt3);
		frame.setVisible(true);
		frame.setResizable(false);

	}
	
	//Functii care permit adaugarea de listenere si accesul la VIEW din afara clasei VIEW
		public void addbtnFristPolynomialOk(ActionListener acl)
		{
			btnFristPolynomialOk.addActionListener(acl);
		}
		
		public void addbtnSecondPolynomialOkListener(ActionListener acl)
		{
			btnSecondPolynomialOk.addActionListener(acl);
		}
		
		public void addbtnAddPolynomialListener(ActionListener acl)
		{
			btnAddPolynomial.addActionListener(acl);
		}
		
		public void addbtnSubPolynomial1Listener(ActionListener acl)
		{
			btnSubPolynomial1.addActionListener(acl);
		}
		
		public void addbtnSubPolynomial2Listener(ActionListener acl)
		{
			btnSubPolynomial2.addActionListener(acl);
		}
		
		public void addbtnMultiplyListener(ActionListener acl)
		{
			btnMultiply.addActionListener(acl);
		}
		
		public void addbtnDivide1Listener(ActionListener acl)
		{
			btnDivide1.addActionListener(acl);
		}
		
		public void addbtnDivide2Listener(ActionListener acl)
		{
			btnDivide2.addActionListener(acl);
		}
		
		public void addbtnIntegration1Listener(ActionListener acl)
		{
			btnIntegration1.addActionListener(acl);
		}
		
		public void addbtnIntegration2Listener(ActionListener acl)
		{
			btnIntegration2.addActionListener(acl);
		}
		
		public void addbtnDerivation1Listener(ActionListener acl)
		{
			btnDerivation1.addActionListener(acl);
		}
		
		public void addbtnDerivation2istener(ActionListener acl)
		{
			btnDerivation2.addActionListener(acl);
		}
		
		public void addbtnValue1Listener(ActionListener acl)
		{
			btnValue1.addActionListener(acl);
		}
		
		public void addbtnValue2Listener(ActionListener acl)
		{
			btnValue2.addActionListener(acl);
		}
		
		public String getFirstPolynomial()
		{
			return firstPolynomial.getText(); // functia getText() returneaza un String cu textu; continut de JTextField-ul firstPolynomial
		}
		
		public String getSecondPolynomial()
		{
			return secondPolynomial.getText(); // functia getText() returneaza un String cu textu; continut de JTextField-ul secPoly
		}
		
		public void setFirstPolynomial(String s)
		{
			viewFirstPolynomial.setText(s); //functie cu care introducem date in primul polinom citibil 
		}
		
		public void setSecondPolynomial(String s)
		{
			viewSecondPolynomial.setText(s); //functie cu care introducem date in primul polinom citibil 
		}
		
		public void setResultPolynomial(String s)
		{
			resultPolyonomial.setText(s);
		}
		
		public void setRemainderPolynomial(String s)
		{
			remainderPolynomial.setText(s);
		}
		
		public int getPolynomialPoint()
		{
			return Integer.parseInt( pointPolynomial.getText() ); // Integer.parseInt transforma un string intr-un integer (daca se poate)
		}	
}
