package util;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.Timer;

import model.Shop;

@SuppressWarnings("serial")
public class PaintShop extends JPanel implements ActionListener {

    private Timer timer;
    private Shop shop;

    public PaintShop(Shop shop)
    {
        this.shop = shop;
        this.timer = new Timer(500, this);
    }

    @Override
    public void paintComponent(Graphics g)
    {
        super.paintComponent(g);
        ImageIcon client = new ImageIcon("client.png");
        ImageIcon checkout = new ImageIcon("checkout.png");

        setPreferredSize(new Dimension(5 + shop.getLongestQueueLength() * 60, 5 + shop.getQueues().size() * 75));
        revalidate();

        for (int i = 0 ; i < shop.getQueues().size(); i++)
        {
            checkout.paintIcon(this, g, 5, 5 + 75 * i);
            g.drawString("[" + (i + 1) + "]", 15, 50 + 75 * i);
            for (int j = 0; j < shop.getQueues().get(i).getClients().size(); j++)
            {
                client.paintIcon(this, g, 5 + 60 * (j + 1), 5 + 75 * i);
                g.drawString(shop.getQueues().get(i).getClients().get(j).toString(), 5 + 60 * (j + 1), 50 + 75 * i);
            }
        }
        timer.start();
    }

    @Override
    public void actionPerformed(ActionEvent e)
    {
        if (shop.isAlive())
        {
            repaint();
        }
        setPreferredSize(new Dimension(10, 5 + shop.getQueues().size() * 42));
        repaint();
    }
}
