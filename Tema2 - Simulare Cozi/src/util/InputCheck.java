package util;

import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class InputCheck implements KeyListener{

    private String exceptions;

    public InputCheck()
    {
        this.exceptions = "";
    }
    public InputCheck(String exceptions)
    {
        this.exceptions = exceptions;
    }

    @Override
    public void keyTyped(KeyEvent e) {
        char c = e.getKeyChar();
        if (!(Character.isDigit(c) || c == KeyEvent.VK_BACK_SPACE || c == KeyEvent.VK_DELETE || exceptions.contains(String.valueOf(c))))
        {
            Toolkit.getDefaultToolkit().beep();
            e.consume();
        }
    }

    @Override
    public void keyPressed(KeyEvent e) {
    }

    @Override
    public void keyReleased(KeyEvent e) {
    }
}