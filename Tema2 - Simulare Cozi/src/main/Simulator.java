package main;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingUtilities;
import javax.swing.SwingWorker;

import net.miginfocom.swing.MigLayout;
import model.Shop;
import util.PaintShop;
import util.InputCheck;

@SuppressWarnings("serial")
public class Simulator extends JFrame {

    private JLabel[] parameterLabels;
    private JTextField[] parameterFields;
    private JButton startButton, stopButton;
    private JScrollPane shopArea, logArea;
    private JTextArea logField;
    private PaintShop paintShop;
    private Shop shop;

    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            public void run()
            {
                try
                {
                    Simulator shopSimulator = new Simulator("Tema 2 - Queues");
                    shopSimulator.setVisible(true);
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
        });
    }

    public Simulator(String title)
    {
        super(title);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(100, 100, 800, 600);
        //setResizable(false);
        initialize();
        addActionListeners();
    }

    private void initialize()
    {
        parameterLabels = new JLabel[7];
        parameterLabels[0] = new JLabel("Opening hour (h):");
        parameterLabels[1] = new JLabel("Closing hour (h):");
        parameterLabels[2] = new JLabel("Number of queues:");
        parameterLabels[3] = new JLabel("Minimum arrival time (m):");
        parameterLabels[4] = new JLabel("Maximum arrival time (m):");
        parameterLabels[5] = new JLabel("Minimum service time (m):");
        parameterLabels[6] = new JLabel("Maximum service time (m):");

        parameterFields = new JTextField[7];
        for (int i = 0; i < parameterFields.length; i++)
            parameterFields[i] = new JTextField();

        startButton = new JButton("Start");
        stopButton = new JButton("Stop");
        stopButton.setEnabled(false);

        shopArea = new JScrollPane(new JPanel());

        logField = new JTextArea();
        logField.setEditable(false);
        logArea = new JScrollPane(logField);
        logArea.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        logArea.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);

        getContentPane().setLayout(new MigLayout("insets 5 5 5 5", "[grow]5[]5[25!]", "[]5[]5[]5[]5[]5[]5[]5[grow]5[grow]"));

        for (int i = 0; i < parameterLabels.length; i++)
            getContentPane().add(parameterLabels[i], "cell 1 " + i + ", right");

        for (int i = 0; i < parameterFields.length; i++)
        {
            parameterFields[i].addKeyListener(new InputCheck());
            getContentPane().add(parameterFields[i], "cell 2 " + i + ", grow");
        }

        getContentPane().add(shopArea, "cell 0 0, span 0 8, grow");
        getContentPane().add(logArea, "cell 0 8, grow");

        getContentPane().add(startButton, "cell 1 7, span 2, split 2, growx, top");
        getContentPane().add(stopButton, "cell 1 7, growx, top");

    }

    private void addActionListeners()
    {
        startButton.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                logField.setText(null);
                for (int i = 0; i < parameterFields.length; i++)
                    if (parameterFields[i].getText().isEmpty())
                        return;
                for (int i = 0; i < parameterFields.length; i++)
                    parameterFields[i].setEditable(false);
                startButton.setEnabled(false);
                stopButton.setEnabled(true);
                int openHour = Integer.parseInt(parameterFields[0].getText());
                int closeHour = Integer.parseInt(parameterFields[1].getText());
                int numberOfQueues = Integer.parseInt(parameterFields[2].getText());
                int minArrivalTime = Integer.parseInt(parameterFields[3].getText());
                int maxArrivalTime = Integer.parseInt(parameterFields[4].getText());
                int minServingTime = Integer.parseInt(parameterFields[5].getText());
                int maxServingTime = Integer.parseInt(parameterFields[6].getText());
                shop = new Shop(openHour, closeHour, numberOfQueues, minArrivalTime, maxArrivalTime, minServingTime, maxServingTime);
                shop.start();

                paintShop = new PaintShop(shop);
                shopArea.setViewportView(paintShop);

                new SwingWorker<Void, Void>() {
                    @Override
                    public Void doInBackground()
                    {
                        while (shop.isAlive())
                        {
                            if (!shop.getLog().isEmpty())
                            {
                                logField.setText(shop.getLog());
                            }
                        }
                        logField.setText(shop.getLog());
                        return null;
                    }

                    @Override
                    public void done()
                    {
                        for (int i = 0; i < parameterFields.length; i++)
                            parameterFields[i].setEditable(true);
                        startButton.setEnabled(true);
                        stopButton.setEnabled(false);

                        shopArea.revalidate();
                    }
                }.execute();
            }
        });

        stopButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (shop.isAlive())
                    shop.exit();
            }
        });
    }
}
