package model;

import java.util.LinkedList;

public class Queue extends Thread {

    private boolean runningState = true;
    private LinkedList<Client> clients;
    private StringBuilder log;

    public Queue()
    {
        this.clients = new LinkedList<>();
        this.log = new StringBuilder();
    }

    public LinkedList<Client> getClients()
    {
        return clients;
    }

    public void enqueue(Client client)
    {
        clients.add(client);
    }

    public void dequeue()
    {
        clients.remove();
    }

    public int getTotalWaitingTime()
    {
        int time = 0;
        for (Client c : clients)
            time += c.getServiceTime();
        return time;
    }

    public void switchRunningState()
    {
        runningState = !runningState;
    }

    public String getLog()
    {
        return log.toString();
    }

    public void clearLog()
    {
        log.setLength(0);
    }

    @Override
    public void run()
    {
        while(runningState || !clients.isEmpty())
        {
            try
            {
                Thread.sleep(1000);
            }
            catch (InterruptedException e)
            {
                e.printStackTrace();
            }
            if (!clients.isEmpty())
            {
                clients.getFirst().decreaseServiceTime();
                if (clients.getFirst().getServiceTime() == 0)
                {
                    log.append("Client #" + clients.getFirst().getId() + " exited after waiting " + clients.getFirst().getWaitingTime() +" minutes.\n");
                    dequeue();
                }
            }
        }
    }
}
