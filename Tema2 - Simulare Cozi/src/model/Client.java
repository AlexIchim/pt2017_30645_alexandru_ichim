package model;

public class Client {

    private int id;
    private int arrivalTime;
    private int serviceTime;
    private int waitingTime;

    public Client(int id, int arrivalTime, int serviceTime)
    {
        this.id = id;
        this.arrivalTime = arrivalTime;
        this.serviceTime = serviceTime;
    }

    public int getArrivalTime()
    {
        return arrivalTime;
    }

    public int getServiceTime()
    {
        return serviceTime;
    }

    public int getWaitingTime()
    {
        return waitingTime;
    }

    public void setWaitingTime(int waitingTime)
    {
        this.waitingTime = waitingTime;
    }

    public int getId()
    {
        return id;
    }

    public void decreaseServiceTime()
    {
        serviceTime--;
    }

    @Override
    public String toString()
    {
        return "[" + id + ":" + arrivalTime + "," + serviceTime +"]";
    }
}
