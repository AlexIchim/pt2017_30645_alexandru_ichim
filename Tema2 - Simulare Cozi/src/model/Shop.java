package model;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

public class Shop extends Thread {

    private int clientId = 1;
    private int openHour;
    private int currentTime = 0;
    private int runningTime;
    private int minArrivalTime, maxArrivalTime;
    private int minServingTime, maxServingTime;
    private ArrayList<Queue> queues;
    private StringBuilder log;

    public Shop(int openHour, int closingHour, int queueNumber, int minArrivalTime, int maxArrivalTime, int minServingTime, int maxServingTime)
    {
        this.openHour = openHour % 24;
        this.runningTime = (closingHour % 24 - openHour % 24) * 60;
        this.minArrivalTime = minArrivalTime;
        this.maxArrivalTime = maxArrivalTime;
        this.minServingTime = minServingTime;
        this.maxServingTime = maxServingTime;
        this.queues = new ArrayList<>(queueNumber);
        for (int i = 0; i < queueNumber; i++)
            queues.add(new Queue());
        log = new StringBuilder();
    }

    public Client generateClient(int maxArrivalTime, int minServingTime, int maxServingTime)
    {
        Random random = new Random();
        int arrivalTime = maxArrivalTime + random.nextInt(this.maxArrivalTime - this.minArrivalTime +1);
        System.out.println("Arrival time: " + arrivalTime + " current time: " + currentTime + " rnd : "  );
        int servingTime = minServingTime + random.nextInt(maxServingTime - minServingTime + 1);
        return new Client(clientId++, arrivalTime, servingTime);
    }

    public LinkedList<Client> addClients(LinkedList<Client> clients, int currentTime, int previousClientArrivalTime)
    {
        while (currentTime == previousClientArrivalTime + minArrivalTime)
        {
            System.out.println(previousClientArrivalTime + minArrivalTime);
            clients.add(generateClient(previousClientArrivalTime + minArrivalTime, minServingTime, maxServingTime));
            previousClientArrivalTime = clients.getLast().getArrivalTime();
        }
        return clients;
    }

    public ArrayList<Queue> getQueues()
    {
        return queues;
    }

    public int getShortestQueue()
    {
        int shortestQueue = 0;
        int minWaitingTime = queues.get(0).getTotalWaitingTime();
        for (int i = 0; i < queues.size(); i++)
            if (queues.get(i).getTotalWaitingTime() < minWaitingTime)
            {
                shortestQueue = i;
                minWaitingTime = queues.get(i).getTotalWaitingTime();
            }
        return shortestQueue;
    }

    public int getLongestQueueLength()
    {
        int maxClients = 0;
        for (Queue q : queues)
            if (q.getClients().size() > maxClients)
            {
                maxClients = q.getClients().size();
            }
        return maxClients;
    }

    public int getClientCount()
    {
        int clientCount = 0;
        for (Queue q : queues)
            clientCount += q.getClients().size();
        return clientCount;
    }

    public String toTime(int time)
    {
        int hours = time / 60;
        int minutes = time % 60;
        return String.format("%02d", hours) + ":" + String.format("%02d", minutes);
    }

    public boolean areQueuesEmpty()
    {
        for (Queue q : queues)
            if (!q.getClients().isEmpty())
                return false;
        return true;
    }

    public void exit()
    {
        currentTime = runningTime;
        for (Queue q : queues)
            q.getClients().clear();
    }

    public String getLog()
    {
        return log.toString();
    }

    @Override
    public void run()
    {
        LinkedList<Client> generatedClients = new LinkedList<>();
        int previousClientArrivalTime = 0, maxClients = 0, totalWaitingTime = 0;
        int peakHour = openHour;

        for (Queue q : queues)
            q.start();

        while (currentTime <= runningTime)
        {
            try
            {
                sleep(1000);
            }
            catch (InterruptedException e)
            {
                e.printStackTrace();
            }

            generatedClients = addClients(generatedClients, currentTime, previousClientArrivalTime);

            if (!generatedClients.isEmpty())
                previousClientArrivalTime = generatedClients.getLast().getArrivalTime();


            while (!generatedClients.isEmpty() && generatedClients.getFirst().getArrivalTime() == currentTime)
            {
                int index = getShortestQueue();
                generatedClients.getFirst().setWaitingTime(queues.get(index).getTotalWaitingTime() + generatedClients.getFirst().getServiceTime());
                queues.get(index).enqueue(generatedClients.getFirst());
                log.append(toTime(openHour*60 + currentTime) + ": " + "Client #" + generatedClients.getFirst().getId() + " entered queue " + (index + 1) + ".\n");
                generatedClients.remove();

                totalWaitingTime += queues.get(index).getTotalWaitingTime();
            }

            if (getClientCount() > maxClients)
            {
                peakHour = currentTime;
                maxClients = getClientCount();
            }

            for (Queue q : queues)
                if (!q.getLog().isEmpty())
                {
                    log.append(toTime(openHour*60 + currentTime) + ": " + q.getLog());
                    q.clearLog();
                }

            currentTime++;
        }

        for (Queue q : queues)
            q.switchRunningState();

        while (!areQueuesEmpty())
        {
            try
            {
                sleep(1000);
            }
            catch (InterruptedException e)
            {
                e.printStackTrace();
            }

            for (Queue q : queues)
                if (!q.getLog().isEmpty())
                {
                    log.append(toTime(openHour*60 + currentTime) + ": " + q.getLog());
                    q.clearLog();
                }

            currentTime++;
        }

        log.append("Average waiting time was: " + String.format("%.2f", (double) totalWaitingTime / clientId) + " minutes\n");
        log.append("Peak hour was at: " + toTime(openHour*60 + peakHour) + " (" + maxClients + " clients)");
    }
}